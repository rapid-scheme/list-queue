;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Mutable list queues compatible with SRFI 117.

(define-library (rapid list-queue)
  (export make-list-queue list-queue list-queue-copy list-queue-unfold list-queue-unfold-right
	  list-queue? list-queue-empty?
	  list-queue-front list-queue-back list-queue-list list-queue-first-last
	  list-queue-add-front! list-queue-add-back! list-queue-remove-front! list-queue-remove-back!
	  list-queue-remove-all! list-queue-set-list!
	  list-queue-append list-queue-append! list-queue-concatenate
	  list-queue-append list-queue-append! list-queue-concatenate
	  list-queue-map list-queue-map! list-queue-for-each)
  (import (scheme base)
	  (scheme case-lambda)
	  (rapid assume)
	  (rapid list))
  (include "list-queue.scm"))
