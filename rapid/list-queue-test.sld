;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid list-queue-test)
  (export run-tests)
  (import (scheme base)
	  (rapid test)
	  (rapid list-queue))
  (begin
    (define (run-tests)
      (test-begin "Mutable queues")

      (test-group "Simple"
	(define x (list-queue 1 2 3))
	(define x1 (list 1 2 3))
	(define x2 (make-list-queue x1 (cddr x1)))
	(define y (list-queue 4 5))
	(define z (list-queue-append x y))
	(define z2 (list-queue-append! (list-queue-copy x) (list-queue-copy y)))

	(test-equal '(1 1 1) (list-queue-list (make-list-queue '(1 1 1))))
	(test-equal '(1 2 3) (list-queue-list x))
	(test-equal 3 (list-queue-back x2))
	(test-assert (list-queue? y))
	(test-equal '(1 2 3 4 5) (list-queue-list z))
	(test-equal '(1 2 3 4 5) (list-queue-list z2))
	(test-equal 1 (list-queue-front z))
	(test-equal 5 (list-queue-back z))
	(list-queue-remove-front! y)
	(test-equal '(5) (list-queue-list y))
	(list-queue-remove-back! y)
	(test-assert (list-queue-empty? y))
	(test-error (list-queue-remove-front! y))
	(test-error (list-queue-remove-back! y))
	(test-equal '(1 2 3 4 5) (list-queue-list z))
	(test-equal '(1 2 3 4 5) (list-queue-remove-all! z2))
	(test-assert (list-queue-empty? z2))
	(list-queue-remove-all! z)
	(list-queue-add-front! z 1)
	(list-queue-add-front! z 0)
	(list-queue-add-back! z 2)
	(list-queue-add-back! z 3)
	(test-equal '(0 1 2 3) (list-queue-list z)))

      (test-group "The whole list queue"
	(define a (list-queue 1 2 3))
	(define b (list-queue-copy a))

	(test-equal '(1 2 3) (list-queue-list b))
	(list-queue-add-front! b 0)
	(test-equal '(1 2 3) (list-queue-list a))
	(test-equal 4 (length (list-queue-list b)))
	(test-equal '(1 2 3 0 1 2 3) (list-queue-list (list-queue-concatenate (list a b))))) 

      (test-group "Mapping"
	(define r (list-queue 1 2 3))
	(define s (list-queue-map (lambda (x) (* x 10)) r))
	(define sum 0)

	(test-equal '(10 20 30) (list-queue-list s))
	(list-queue-map! (lambda (x) (+ x 1)) r)
	(test-equal '(2 3 4) (list-queue-list r))
	(list-queue-for-each (lambda (x) (set! sum (+ sum x))) s)
	(test-equal 60 sum))

      (test-group "Conversion"
	(define n (list-queue 5 6))
	(define d (list 1 2 3))
	(define e (cddr d))
	(define f (make-list-queue d e))
	(define-values (dx ex) (list-queue-first-last f))
	(define g (make-list-queue d e))
	(define h (list-queue 5 6))

	(list-queue-set-list! n (list 1 2))
	(test-equal '(1 2) (list-queue-list n))
	(test-assert (eq? d dx))
	(test-assert (eq? e ex))
	(test-equal '(1 2 3) (list-queue-list f))
	(list-queue-add-front! f 0)
	(list-queue-add-back! f 4)
	(test-equal '(0 1 2 3 4) (list-queue-list f))
	(test-equal '(1 2 3 4) (list-queue-list g))
	(list-queue-set-list! h d e)
	(test-equal '(1 2 3 4) (list-queue-list h)))

      (test-group "list-queues/unfold"
	(define (double x) (* x 2))
	(define (done? x) (> x 3))
	(define (add1 x) (+ x 1))
	(define x (list-queue-unfold done? double add1 0))
	(define y (list-queue-unfold-right done? double add1 0))
	(define x0 (list-queue 8))
	(define x1 (list-queue-unfold done? double add1 0 x0))
	(define y0 (list-queue 8))
	(define y1 (list-queue-unfold-right done? double add1 0 y0))

	(test-equal '(0 2 4 6) (list-queue-list x))
	(test-equal '(6 4 2 0) (list-queue-list y))
	(test-equal '(0 2 4 6 8) (list-queue-list x1))
	(test-equal '(8 6 4 2 0) (list-queue-list y1)))

      (test-end))))
