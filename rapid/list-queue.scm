;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-record-type <list-queue>
  (%make-list-queue first last)
  list-queue?
  (first get-first set-first!)
  (last get-last set-last!))

(define make-list-queue
  (case-lambda
    ((list)
     (if (null? list)
       (%make-list-queue '() '())
       (%make-list-queue list (last-pair list))))
    ((list last)
     (%make-list-queue list last))))

(define (list-queue . objs)
  (make-list-queue objs))

(define (list-queue-copy list-queue)
  (make-list-queue (list-copy (get-first list-queue))))

(define (list-queue-empty? list-queue)
  (null? (get-first list-queue)))

(define (list-queue-front list-queue)
  (assume (not (list-queue-empty? list-queue))
    "list-queues with a first element are not empty" list-queue)
  (car (get-first list-queue)))

(define (list-queue-back list-queue)
  (assume (not (list-queue-empty? list-queue))
    "list-queues with a last element are not empty" list-queue)
  (car (get-last list-queue)))

(define (list-queue-add-front! list-queue element)
  (let ((new-first (cons element (get-first list-queue))))
    (when (list-queue-empty? list-queue)
      (set-last! list-queue new-first))
    (set-first! list-queue new-first)))

(define (list-queue-add-back! list-queue element)
  (let ((new-last (list element)))
    (if (list-queue-empty? list-queue)
	(set-first! list-queue new-last)
	(set-cdr! (get-last list-queue) new-last))
    (set-last! list-queue new-last)))

(define (list-queue-remove-front! list-queue)
  (assume (not (list-queue-empty? list-queue))
    "list-queues with a first element are not empty" list-queue)
  (let* ((old-first (get-first list-queue))
         (element (car old-first))
         (new-first (cdr old-first)))
    (if (null? new-first)
      (set-last! list-queue '()))
    (set-first! list-queue new-first)
    element))

(define (list-queue-remove-back! list-queue)
  (assume (not (list-queue-empty? list-queue))
    "list-queues with a last element are not empty" list-queue)
  (let* ((old-last (get-last list-queue))
         (element (car old-last))
         (new-last (next-to-last-pair (get-first list-queue))))
    (if (null? new-last)
	(set-first! list-queue '())
	(set-cdr! new-last '()))
    (set-last! list-queue new-last)
    element))

(define (list-queue-remove-all! list-queue)
   (let ((result (get-first list-queue)))
      (set-first! list-queue '())
      (set-last! list-queue '())
      result))

(define (next-to-last-pair list)
  (let loop ((list list))
    (cond
     ((null? (cdr list)) '())
     ((null? (cddr list)) list)
     (else (loop (cdr list))))))

(define (list-queue-append . list-queues)
  (list-queue-concatenate list-queues))

(define (list-queue-concatenate list-queues)
  (let ((result (list-queue)))
    (for-each
      (lambda (list-queue)
        (for-each (lambda (element) (list-queue-add-back! result element)) (get-first list-queue)))
      list-queues)
     result))

(define list-queue-append!
  (case-lambda
    (()
     (list-queue))
    ((queue)
     queue)
    (queues
     (for-each (lambda (queue)
		 (list-queue-join! (car queues) queue))
	       (cdr queues))
     (car queues))))

(define (list-queue-join! queue1 queue2)
  (set-cdr! (get-last queue1) (get-first queue2)))

(define (list-queue-map proc list-queue)
  (make-list-queue (map proc (get-first list-queue))))

(define list-queue-unfold
  (case-lambda
    ((stop? mapper successor seed)
     (list-queue-unfold stop? mapper successor seed (list-queue)))
    ((stop? mapper successor seed queue)
     (let loop ((seed seed))
       (if (not (stop? seed))
	   (list-queue-add-front! (loop (successor seed)) (mapper seed)))
       queue))))

(define list-queue-unfold-right
  (case-lambda
    ((stop? mapper successor seed)
     (list-queue-unfold-right stop? mapper successor seed (list-queue)))
    ((stop? mapper successor seed queue)
     (let loop ((seed seed))
       (if (not (stop? seed))
	   (list-queue-add-back! (loop (successor seed)) (mapper seed)))
       queue))))

(define (list-queue-map! proc list-queue)
  (let loop ((list (get-first list-queue)))
    (when (pair? list)
      (set-car! list (proc (car list)))
      (loop (cdr list)))))

(define (list-queue-for-each proc list-queue)
  (for-each proc (get-first list-queue)))

(define (list-queue-list list-queue)
  (get-first list-queue))

(define (list-queue-first-last list-queue)
  (values (get-first list-queue) (get-last list-queue)))

(define list-queue-set-list!
  (case-lambda
    ((list-queue first)
     (set-first! list-queue first)
     (if (null? first)
	 (set-last! list-queue '())
	 (set-last! list-queue (last-pair first))))
    ((list-queue first last)
     (set-first! list-queue first)
     (set-last! list-queue last))))
